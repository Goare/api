class PostsController < ApplicationController
  def index
    render json: Post.all
  end

  def create
    create_params = params.permit(:title, :body, :user_id)

    p = Post.create(create_params)

    render json: p if p.valid?
  end

  def show
    render json: Post.find(params[:id])
  end

  def update
    p = Post.find(params[:id])

    update_params = params.permit(:title, :body)

    if p.update(update_params)
      render json: p
    end
  end

  def destroy
    p = Post.find(params[:id])
    p.destroy

    render json: p
  end

end
