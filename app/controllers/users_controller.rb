class UsersController < ApplicationController
  def index
    render json: User.all
  end

  def create
    create_params = params.permit(:email, :username, :password)

    u = User.create(create_params)
    
    render json: u if u.valid?
  end

  def show
    render json: User.find(params[:id])
  end

  def update
    u = User.find(params[:id])

    update_params = params.permit(:password)

    if u.update(update_params)
      render json: u
    end
  end

  def destroy
    u = User.find(params[:id])
    u.destroy

    render json: u
  end
end
